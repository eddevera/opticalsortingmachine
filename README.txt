SDSU Spring 2015 CS583 3D Game Programming

Assignment #5 Independent 2D Game

Eduardo Devera 813849589

Game Title: Optical Sorting Machine

Description:
	The player must sort bombs into their corresponding bins
	in order to achieve a high score.
	
	The more bombs the player sorts the faster the bombs are
	distributed.
	
	If the player incorrectly sorts a bomb, or fails to sort
	a bomb, the bin that the bomb was placed into will be 
	destroyed and will be excluded from the player's score.
	Unsorted bombs are simply destroyed, and the game ends
	in a similar fashion.
	
	Each bin is automatically scored every 15 bombs.
	All bins are scored when the game ends.
	
How To Play:
	If One-Click Move is enabled bombs may be moved with either the left mouse button, or the right mouse button.
	LMB: Sends the bomb to the black bin.
	RMB: Sends the bomb to the red bin.
	
	If One-Click Move is not enabled, the standard way to move the bombs is to simply click and drag them into the bin.
	
		One-Click Move behavior can still be achieved by holding shift.
		Alternatively:
			If 'A' is held when the bomb is clicked the bomb will be sent to the black bin.
			If 'D' is held when the bomb is clicked the bomb will be sent to the red bin.

--

Known Bugs:
	The high score reset button doesn't always update the text on the end screen.
		It is functional however.

General Reference:
	http://docs.unity3d.com/ScriptReference/
	
Borrowed Assets:
	Assets/Scripts/Objects/GameplayCamera.cs
		http://gamedesigntheory.blogspot.ie/2010/09/controlling-aspect-ratio-in-unity.html
		
Other Assets:
	All 2D assets were produced with Adobe Photoshop CS6.
	All sound assets were produced with Audacity. 
	All script assets were produced with the Unity Editor/MonoDevelop
