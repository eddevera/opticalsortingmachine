﻿/* Controls the scene lights for Arena_Standard.
 * Moves the light between two positions repeatedly.
 * 
 * The code should've worked for other scenes/objects but it doesn't.
 */

using UnityEngine;
using System.Collections;

public class Waver : MonoBehaviour 
{
	Vector2 initialPosition;
	Vector2 min;
	Vector2 max;
	Vector2 lerp;
	void Start()
	{
		initialPosition = gameObject.transform.position;
		min = new Vector2 (initialPosition.x - 1, initialPosition.y);
		max = new Vector2 (initialPosition.x + 1, initialPosition.y);
		lerp = Vector2.Lerp (min, max, 10000);
	}

	void Update()
	{
		// If the absolute value of the x position exceeds 10 invert the vector.
		if (gameObject.tag != "paused") 
		{
			if (gameObject.transform.position.x > 10 || gameObject.transform.position.x < -10)
				lerp = -lerp;
			transform.Translate(lerp * Time.deltaTime * 2f);
		}
	}
}
