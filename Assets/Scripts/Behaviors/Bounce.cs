﻿/* Deprecated - Functionality merged into BasicWander.cs
 * Keeping it just in case I guess.
 */

using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour {

	private Vector2 previous;
	private Vector2 current;
	private float degrees;
	
	void Update () 
	{
		if(!(gameObject.tag == "bomb_grabbed"))
		{
			previous = current;
			current = transform.position;
			if(previous.x == current.x) // This means that we're stuck on a wall.
			{
				if (degrees >= 0)
					degrees = 180 - degrees;
				else
					degrees = -180 - degrees;
				gameObject.SendMessage("SetDirection",degrees);
			}
			if(previous.y == current.y) // This means that we're stuck on the floor or ceiling.
			{
				gameObject.SendMessage("SetDirection",degrees * -1);
			}
			gameObject.SendMessage("Move");
		}
	}
	void Deflect(Vector2 point)
	{
		
		Vector2 center = gameObject.GetComponent<Collider2D> ().bounds.center;
		bool top = false;
		bool bottom = false;
		
		if (point.y > center.y && point.x == center.x) // True-center top hit.
			top = true;
		if (point.y < center.y && point.x == center.x) // True-center bottom hit.
			bottom = true;
		
		if(top || bottom)
			degrees = degrees * -1;
		else if(degrees >= 0)
		{
			if(point.y > center.y)
				degrees = degrees * -1; // Non-center top hit.
			else
				degrees = 180 - degrees; // True-center side hit upper quadrants.
		}
		else
		{
			if(point.y < center.y)
				degrees = degrees * -1; // Non-center bottom hit.
			else
				degrees = -180 - degrees; // True-center side hit lower quadrants.
		}
		gameObject.SendMessage("SetDirection",degrees);
	}
}
