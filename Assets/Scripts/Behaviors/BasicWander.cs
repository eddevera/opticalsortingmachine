﻿/* Controls the basic movement of the bomb objects.
 * When a Wall object detects a collision it invokes Deflect.
 * Deflect changes the direction of the bomb according to the angle it hit the wall at.
 */

using UnityEngine;
using System.Collections;

public class BasicWander : MonoBehaviour {
	
	public Transform spawnPoint;
	
	private Vector2 direction;			// The current direction of the bomb.
	private Vector2 previous;			// We need this and current so we can tell if we're stuck.
	private Vector2 current;
	private float degrees;				// The direction of the bomb expressed in degrees (-180 to 180);

	public float movementMultiplier;	// Modulate Time.DeltaTime because it moves too slow on its own.
	
	void SetMovementMultiplier(float value)
	{
		movementMultiplier = value;
	}
	
	void Start () 
	{
		previous = new Vector2(-1200,-1200);            // So the first bomb doesn't freak out.
		Physics2D.IgnoreLayerCollision (8, 8);          // So that bombs don't collide with eachother.
		Physics.solverIterationCount = 1;               // This probably has no effect but sure.
		SetDirection (degrees+Random.Range(-60,60));    // The angle of the spawner with some variance.
	}
	void SetDirection(float angle)
	{
		degrees = angle;
		angle = angle * Mathf.Deg2Rad;
		if(angle == 0 || angle == 2 || angle == 1)
		{
			// Perfectly horizontal or vertical collision.
			// Do something about it maybe? Or maybe we don't care.
			// We don't care.
		}
		direction.Set ((float)Mathf.Cos (angle), (float)Mathf.Sin (angle));
	}

	void Update () 
	{
		// Move the bomb, or don't move it, depending on its tags.
		if (gameObject.tag != "bomb_grabbed" && gameObject.tag != "paused")
		{
			previous = current;
			current = transform.position;
			if(previous.x == current.x) // This means that we're stuck on a wall.
			{
				if (degrees >= 0)
					degrees = 180 - degrees;
				else
					degrees = -180 - degrees;
				SetDirection(degrees);
			}
			if(previous.y == current.y) // This means that we're stuck on the floor or ceiling.
			{
				SetDirection(degrees * -1);
			}
			transform.Translate (direction * Time.fixedDeltaTime * movementMultiplier);
		}
	}

	void SetStartingRotation(float angle)
	{
		degrees = angle;
	}

	void Deflect(Vector2 point)
	{
		// Determines what part of the bomb touched a wall and responds accordingly.
		Vector2 center = gameObject.GetComponent<Collider2D> ().bounds.center;
		bool top = false;
		bool bottom = false;
		
		if (point.y > center.y && point.x == center.x) // True-center top hit.
			top = true;
		if (point.y < center.y && point.x == center.x) // True-center bottom hit.
			bottom = true;
		
		if(top || bottom)
			degrees = degrees * -1;
		else if(degrees >= 0)
		{
			if(point.y > center.y)
				degrees = degrees * -1; // Non-center top hit.
			else
				degrees = 180 - degrees; // True-center side hit upper quadrants.
		}
		else
		{
			if(point.y < center.y)
				degrees = degrees * -1; // Non-center bottom hit.
			else
				degrees = -180 - degrees; // True-center side hit lower quadrants.
		}
		SetDirection (degrees);
	}
}
