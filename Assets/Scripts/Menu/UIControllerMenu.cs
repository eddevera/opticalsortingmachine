﻿/* Similar to UIController, but strictly for Main_Menu.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIControllerMenu : MonoBehaviour 
{
	public Button buttonHowToPlay;
	public Button buttonPlayGame;
	public Button buttonQuit;
	public Button buttonCredits;
	public Toggle toggleOneClickMove;

	public Button buttonCloseBox;
	public Text textCredits;
	public Text textHowToPlay;

	public Text textTitle;
	public Text textHighScore;

	public GameObject fadeWall;
	public GameObject textPanel;

	public bool ready;

	public void WriteTitle()
	{
		textTitle.GetComponent<Text> ().enabled = true;
		StartCoroutine (TypeInTitle ());
	}
	
	IEnumerator TypeInTitle()
	{
		// Typing effect.
		textTitle.enabled = true;
		ready = false;
		string tmp = "OPTICAL SORTING MACHINE";
		string tmp2 = "";
		char [] msg = tmp.ToCharArray ();
		int i = 0;
		while (tmp2 != tmp) 
		{
			tmp2 = tmp2 + "" + msg[i];
			i++;
			textTitle.text = tmp2;
			yield return new WaitForSeconds(.05f);
		}
		ready = true;
		StartCoroutine (TypeInHighScore ());
	}

	IEnumerator TypeInHighScore()
	{
		// Typing effect.
		textHighScore.enabled = true;
		ready = false;
		string tmp = "Current High Score: ";
		string tmp2 = "";
		char [] msg = tmp.ToCharArray ();
		int i = 0;
		while (tmp2 != tmp) 
		{
			tmp2 = tmp2 + "" + msg[i];
			i++;
			textHighScore.text = tmp2;
			yield return new WaitForSeconds(.05f);
		}
		textHighScore.text = textHighScore.text + PlayerPrefs.GetInt ("highScore");
		ready = true;
	}


	IEnumerator WaitForFade()
	{
		// Stop the player from clicking play before the scene has faded in completely.
		while (!fadeWall.GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame ();
		}
		fadeWall.GetComponent<FadeWall> ().CompleteIntroFade ();
		while (!fadeWall.GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame ();
		}
		buttonPlayGame.enabled = true;
	}

	public void PlayGame()
	{
		StopCoroutine (WaitForFade ());
		StartCoroutine (StartGameSequence ());
	}
	
	IEnumerator StartGameSequence()
	{
		fadeWall.GetComponent<FadeWall> ().FadeOut ();
		while (!fadeWall.GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame();
		}
		Application.LoadLevel ("Arena_Standard");
	}

	public void Quit()
	{
		Application.Quit ();
	}

	public void CloseAllBoxes()
	{
		// So both messages don't show up at once, ensure that we close all messages before opening a new one.
		buttonCloseBox.enabled = false;
		buttonCloseBox.image.enabled = false;
		buttonCloseBox.GetComponentInChildren<Text> ().enabled = false;

		textCredits.enabled = false;
		textHowToPlay.enabled = false;
		textPanel.GetComponent<Renderer> ().enabled = false;
	}

	public void HowToPlay()
	{
		// Display the how to play message.
		CloseAllBoxes ();
		buttonCloseBox.enabled = true;
		buttonCloseBox.image.enabled = true;
		buttonCloseBox.GetComponentInChildren<Text> ().enabled = true;

		textHowToPlay.enabled = true;
		textPanel.GetComponent<Renderer> ().enabled = true;
	}

	public void Credits()
	{
		// Display the credits.
		CloseAllBoxes ();
		buttonCloseBox.enabled = true;
		buttonCloseBox.image.enabled = true;
		buttonCloseBox.GetComponentInChildren<Text> ().enabled = true;
		
		textCredits.enabled = true;
		textPanel.GetComponent<Renderer> ().enabled = true;
	}

	void Start()
	{
		StartCoroutine (TypeInTitle ());
		ready = true;
		fadeWall.GetComponent<Renderer> ().enabled = true;
		buttonPlayGame.enabled = false;
		StartCoroutine (WaitForFade());
		CloseAllBoxes ();

		// Get the player's previous preference for one-click moving.
		if (PlayerPrefs.HasKey ("oneClickMove")) {
			if (PlayerPrefs.GetInt ("oneClickMove") != 0) 
				toggleOneClickMove.isOn = true;
		} 
		else 
		{
			toggleOneClickMove.isOn = false;
			PlayerPrefs.SetInt ("oneClickMove", 0);
		}
	}

	public void HandleToggle()
	{
		if(toggleOneClickMove.isOn)
			PlayerPrefs.SetInt("oneClickMove",1);
		else
			PlayerPrefs.SetInt("oneClickMove",0);
	}
}
