﻿/* Spawns bombs.
 */

using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	public GameObject targetObject;
	public Transform spawnPoint;

	static Quaternion dummyAngle = new Quaternion ();
	static Color colorBlack = new Color(0.24f,0.24f,0.24f,1f);

	public void Spawn(string type)
	{
		// Spawn a bomb of the given time, originating from the spawner.
		if (gameObject.tag != "paused") 
		{
			StartCoroutine(Jerk ());
			GameObject newObject = Instantiate(targetObject, spawnPoint.position, dummyAngle) as GameObject;
			newObject.SendMessage ("SetStartingRotation", spawnPoint.eulerAngles.z - 90);
			newObject.GetComponent<Rigidbody2D> ().WakeUp ();
			// This needs a more generic solution, but we're strapped for time so it's not happening.
			if (type == "Red") 
			{
				newObject.SendMessage("Initiate",type);
				newObject.GetComponent<SpriteRenderer> ().color = Color.red;
			} 
			else if (type == "Black")
			{
				newObject.SendMessage("Initiate",type);
				newObject.GetComponent<SpriteRenderer> ().color = colorBlack;
			}
			newObject.GetComponent<Rigidbody2D>().WakeUp();
			gameObject.GetComponent<AudioSource> ().Play ();
		}
	}

	IEnumerator Jerk()
	{
		// Simulate recoil.
		SpriteRenderer tmp = gameObject.GetComponent<SpriteRenderer> ();
		for (int i = 0; i<3; i++) 
		{
			tmp.transform.Translate (Vector2.up);
			yield return new WaitForFixedUpdate();
		}
		for (int i = 0; i<3; i++) 
		{
			tmp.transform.Translate (Vector2.up*-1);
			yield return new WaitForFixedUpdate();
		}
	}
}
