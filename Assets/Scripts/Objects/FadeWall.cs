﻿/* Controls the black quad that creates the fading effect.
 */

using UnityEngine;
using System.Collections;

public class FadeWall : MonoBehaviour 
{
	Color tmp = new Color (0f, 0f, 0f, 1f);

	public bool ready = false;	// This allows us to wait on this script in other scripts.

	void Start()
	{
		gameObject.GetComponent<MeshRenderer> ().material.color = tmp;
		StartCoroutine (IntroFade());
	}

	public void Restart()
	{
		Start ();
	}

	IEnumerator IntroFade()
	{
		// Fade to .6 opacity and wait.
		ready = false;
		while (tmp.a > .6f) 
		{
			tmp.a = tmp.a - .01f;
			gameObject.GetComponent<MeshRenderer>().material.color = tmp;
			yield return new WaitForEndOfFrame();
		}
		ready = true;
	}

	public void FadeOut()
	{
		gameObject.GetComponent<Renderer> ().enabled = true;
		StartCoroutine(OuttroFade());
	}
	IEnumerator OuttroFade()
	{
		// Fade in to 100% opacity (opaque).
		ready = false;
		while(tmp.a < 1f)
		{
			tmp.a = tmp.a + .01f;
			gameObject.GetComponent<MeshRenderer>().material.color = tmp;
			yield return new WaitForEndOfFrame();
		}
		ready = true;
	}

	public void CompleteIntroFade()
	{
		StartCoroutine (IntroFadeRest());
	}

	IEnumerator IntroFadeRest()
	{
		// Fade out completely (transparent).
		ready = false;
		while (tmp.a > 0f) 
		{
			tmp.a = tmp.a - .01f;
			gameObject.GetComponent<MeshRenderer>().material.color = tmp;
			yield return new WaitForEndOfFrame();
		}
		gameObject.GetComponent<Renderer> ().enabled = false;
		// print (tmp.a);
		ready = true;
	}
}
