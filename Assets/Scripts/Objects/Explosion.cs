﻿/* Destroys the sprite when the animation has concluded.
 */

using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	
	void Start () 
	{
		//StartCoroutine(Explode());
	}

	/*
	IEnumerator Explode()
	{
		GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length - .01f);
	}
	*/

	void Suicide()
	{
		Destroy(gameObject);
	}

	// This code makes bombs in proximity explode, but we won't be handling it like this for this game.
	/*
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.CompareTag("bomb_armed"))
		{
			col.gameObject.SendMessage("Explode");
		}
		GetComponent<Collider2D>().enabled = false;
	}
	*/
}
