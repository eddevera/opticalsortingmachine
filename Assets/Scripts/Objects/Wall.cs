﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour 
{
	// Invokes deflect.
	// If a collider doesn't have this on it the bomb will get stuck on it.
	void OnCollisionEnter2D(Collision2D col)
	{
		col.gameObject.SendMessage ("Deflect", col.contacts [0].point);
	}
	void OnCollisionStay2D(Collision2D col)
	{
		col.gameObject.SendMessage ("Deflect", col.contacts [0].point);
	}
}
