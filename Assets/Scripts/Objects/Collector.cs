﻿/* Controls animations and sounds for the collector objects.
 */

using UnityEngine;
using System.Collections;

public class Collector : MonoBehaviour {

	Animator flip;

	public AudioClip soundOpen;
	public AudioClip soundClose;

	void PlaySound(AudioClip sound)
	{
		AudioSource tmp = gameObject.GetComponent<AudioSource> ();
		tmp.clip = sound;
		tmp.Play ();
	}

	void Start()
	{
		flip = gameObject.GetComponent<Animator>();
	}

	public void Open()
	{
		flip.Play ("Open");
		PlaySound(soundOpen);
	}

	public void Close()
	{
		flip.Play ("Close");
		PlaySound(soundClose);
	}
}
