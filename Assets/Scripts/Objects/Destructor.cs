﻿/* Used to destroy scored bombs.
 */

using UnityEngine;
using System.Collections;

public class Destructor : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col)
	{
			Destroy (col.gameObject);
	}
}
