﻿/* Script for game areas.
 * An area can be scored and cleared.
 * Areas tag objects that enter them with a tag that matches the area's type.
 */

using UnityEngine;
using System.Collections;

public class AreaGeneric : MonoBehaviour 
{
	public string areaType;
	public Transform collectorTarget;
	public GameObject collector;
	public AudioClip soundPullBomb;
	public int scoreEvery;

	public int count;

	// public ArrayList members = new ArrayList(); /* Old system, deprecated. */

	void OnTriggerEnter2D(Collider2D col)
	{
		// An object entered the area, tag it with our tag.
		col.gameObject.GetComponent<Taggable> ().Tag (areaType);
		count++;
	}

	void Update()
	{
		// Check if we have enough bombs to score the area.
		if(!name.Contains("Arena"))
		if (count >= scoreEvery) 
		{
			StartCoroutine(ScoreClear());
			count = 0;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		count--;
	}

	void Clear()
	{
		Taggable [] bombs = GameObject.FindObjectsOfType<Taggable> ();
		for (int i = 0; i < bombs.Length; i++) 
		{
			if(bombs[i].tagType == areaType)
				bombs[i].SendMessage("Explode");
		}
		count = 0;
	}

	public void Score()
	{
		StartCoroutine (ScoreClear ());
	}

	IEnumerator ScoreClear()
	{
		// Get all the bomb objects.
		// Of those bomb objects operate on only the bomb objects in our area.
		Taggable [] bombs = GameObject.FindObjectsOfType<Taggable> ();
		collector.GetComponent<Collector> ().Open ();	// Animates the collector object.
		yield return new WaitForSeconds (1.3f);			// Wait for animation to complete.
		for(int i = 0; i < bombs.Length; i++)
		{
			if(bombs[i] != null)
			{
				if(bombs[i].myType == areaType && bombs[i].tagType == areaType)
				{
					GameObject.Find ("Score").GetComponent<Score>().AddPoint();		// This was a valid bomb in our area, score it.
					PlaySound(soundPullBomb);
					GameObject obj = bombs[i].gameObject;
					obj.GetComponent<Collider2D>().enabled = false;

					// Pull the bomb towards the collector.
					while (obj != null) 
					{
						if((obj.transform.position.x - collectorTarget.position.x < 1) && (obj.transform.position.y - collectorTarget.position.y < 1))
							obj.GetComponent<Collider2D>().enabled = true;
						Vector2 direction = new Vector2(collectorTarget.position.x-obj.transform.position.x,collectorTarget.position.y-obj.transform.position.y);
						obj.transform.Translate(direction * Time.deltaTime * 10);
						yield return new WaitForEndOfFrame();
					}
				}
			}
		}
		collector.GetComponent<Collector> ().Close ();
	}

	void PlaySound(AudioClip sound)
	{
		AudioSource tmp = gameObject.GetComponent<AudioSource> ();
		tmp.clip = sound;
		tmp.Play ();
	}
}
