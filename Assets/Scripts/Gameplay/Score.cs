﻿/* Score tracking and high score management.
 * Was having issues tracking score with GameManager, so I moved all score tracking here.
 */

using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	private static int score;
	public bool newHigh = false;

	void Start () 
	{
		score = 0;
		newHigh = false;
	}

	public void AddPoint()
	{
		score++;
	}

	public void Reset()
	{
		score = 0;
	}

	public void ResetHighScore()
	{
		PlayerPrefs.SetInt ("highScore", 0);
	}

	public int GetScore()
	{
		return score;
	}

	public int GetHighScore()
	{
		// Get the current high score, if there isnt one set it to 0 and return 0.
		if (PlayerPrefs.HasKey ("highScore")) 
		{
			return PlayerPrefs.GetInt("highScore");
		}
		PlayerPrefs.SetInt ("highScore", 0);
		return 0;
	}
	public bool CheckScore()
	{
		// If our score is greater than the current high score make our score the high score.
		// Then signal whether or not the high score was broken.
		if (score > PlayerPrefs.GetInt ("highScore")) 
		{
			PlayerPrefs.SetInt ("highScore", score);
			return true;
		}
		return false;
	}
}
