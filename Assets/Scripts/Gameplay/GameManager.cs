﻿/* Controls Starting, Stopping, Scoring, and Spawning.
 */

using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	bool playing = false;

	void Update()
	{
		if(Input.GetKeyUp(KeyCode.Escape) && playing)
			HandlePause ();
	}

	void HandlePause()
	{
		if (paused) 
		{
			GameObject.Find ("UIController").GetComponent<UIController>().pausedText.enabled = false;
			paused = false;
			Unpause ();
		} 
		else 
		{
			GameObject.Find ("UIController").GetComponent<UIController>().pausedText.enabled = true;
			paused = true;
			Pause ();
		}
	}

	public int bombsDefused = 0;
	
	public bool gameOver = false;
	public bool paused = false;
	public bool lossScored = false;
	
	public float initalDelay;
	public float delay;
	
	public int rampIntervalInitial;		// The number of bombs before the spawn rate increases.
	public int rampIntervalJump;		// How much the ramp interval will be increased per cycle.
	public int rampInterval;			// The current ramp interval.

	int cycle = 1;						// The current cycle;
	
	Spawner topSpawner;					// We need this and bottomSpawner for the scripted spawning sequence.
	Spawner bottomSpawner;
	
	public AudioClip soundExplosion;

	public void StartGame()
	{
		playing = false;
		bombsDefused = 0;
		cycle = 1;
		lossScored = false;
		delay = initalDelay;
		rampInterval = rampIntervalInitial;
		gameOver = false;
		paused = false;
		StartCoroutine (InitialSpawning ());
		GameObject.Find ("Score").GetComponent<Score> ().Reset ();
	}
	
	IEnumerator InitialSpawning()
	{
		// Scripted spawning phase, show the player how things work.
		// We check for gameOver at every spawn here because it's not handled like it is in automatic spawning.

		playing = true;
		Spawner [] spawners = GameObject.FindObjectsOfType<Spawner> ();
		
		for (int i = 0; i < spawners.Length; i++) 
		{
			if(gameOver) 
				break;
			if(spawners[i].name.Contains("Top"))
				topSpawner = spawners[i];
			else
				bottomSpawner = spawners[i];
		}
		for (int i = 0; i < 3; i++) 
		{
			if(gameOver) 
				break;
			topSpawner.Spawn ("Black");
			yield return new WaitForSeconds(delay);
		}
		if (!gameOver) 
		{
			bottomSpawner.Spawn ("Red");
			yield return new WaitForSeconds (delay);
			StartCoroutine (GenericSpawning ());
		}
	}
	
	IEnumerator GenericSpawning()
	{
		/* Unscripted Spawning, every "rampInterval" bombs, decrease spawn delay by spawn delay/3.
		 * The ramp interval is then increased by the ramp interval jump value * the cycle number.
		 * This gives the player more time to work with the faster pace. Otherwise the player
		 * would be overwealmed very easily.
		 */
		Spawner [] spawners = GameObject.FindObjectsOfType<Spawner> ();
		string [] types = {"Black","Red"};
		while (true) 
		{
			if(gameOver) 
				break;
			if (bombsDefused >= rampInterval)
			{
				bombsDefused = 0;
				rampInterval = rampInterval + rampIntervalJump * cycle;
				delay = delay - delay/3;
				cycle++;
			}
			if(!paused)
			{
				spawners[BinaryRandom ()].Spawn(types[BinaryRandom ()]);
				yield return new WaitForSeconds(delay);
			}
			else
				yield return new WaitForSeconds(0);
		}
	}

	int BinaryRandom()
	{
		// Random.Range() between 0 and 1 wasn't working as expected, so we generate a range and check the value instead.
		if (Random.Range (-10, 10) < 0)
			return 0;
		return 1;
	}
	
	void Lose(string tagType)
	{
		playing = false;
		Debug.Log ("Loss State Triggered by: " + tagType);

		// Destroy all the bombs in the area that triggered the loss state, and all the bombs in the arena.
		// Then score the remaining bombs.

		gameOver = true;
		GameObject [] areas = GameObject.FindGameObjectsWithTag ("area");
		for(int i = 0; i < areas.Length; i++)
		{
			if(areas[i].name.Contains(tagType) || areas[i].name.Contains("Arena"))
				areas[i].SendMessage("Clear");
		}
		PlaySound (soundExplosion);
		for(int i = 0; i < areas.Length; i++)
		{
			if(!areas[i].name.Contains ("Arena"))
			{
				areas[i].GetComponent<AreaGeneric>().Score();
			}
		}
		StartCoroutine (WaitForScoring ());
	}
	
	IEnumerator WaitForScoring()
	{
		// Aesthetic purposes, wait for all the bombs to be cleared from the bins to display the score.
		while (true) 
		{
			Explosive [] bombs = GameObject.FindObjectsOfType<Explosive>();
			if(bombs.Length <= 0)
				break;
			yield return new WaitForEndOfFrame();
		}
		GameObject.Find ("UIController").GetComponent<UIController> ().WriteEndScore ();
		while (!GameObject.Find("UIController").GetComponent<UIController>().endReady) 
		{
			yield return new WaitForEndOfFrame();
		}
		GameObject.Find ("UIController").GetComponent<UIController> ().EndGame ();
		lossScored = true;
	}
	
	void PlaySound(AudioClip sound)
	{
		AudioSource tmp = gameObject.GetComponent<AudioSource> ();
		tmp.clip = sound;
		tmp.Play ();
	}

	// I haven't implemented pause yet, but the plan is to have it triggered by Esc or something.

	public void Pause()
	{
		Pausable [] pausables = GameObject.FindObjectsOfType<Pausable> ();
		for(int i = 0; i < pausables.Length; i++)
		{
			pausables[i].gameObject.SendMessage("Pause");
		}
	}
	
	public void Unpause()
	{
		Pausable [] pausables = GameObject.FindObjectsOfType<Pausable> ();
		for(int i = 0; i < pausables.Length; i++)
		{
			pausables[i].gameObject.SendMessage("Unpause");
		}
	}
}
