﻿/* Manages buttons/text for Arena_Standard.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIController : MonoBehaviour {

	// Use this for initialization

	public Button startButton;
	public Button replayButton;
	public Button resetScoreButton;
	public Button menuButton;

	public Text finalScoreText;
	public Text highScoreText;
	public Text pausedText;

	public bool endReady = false;

	void Start () 
	{
		pausedText.enabled = false;

		GameObject.Find ("FadeWall").GetComponent<Renderer> ().enabled = true;
		endReady = false;

		startButton.gameObject.GetComponent<Image> ().enabled = false;
		startButton.enabled = false;
		StartCoroutine (WaitForFade ());

		replayButton.image.enabled = false;
		replayButton.enabled = false;

		menuButton.image.enabled = false;
		menuButton.enabled = false;

		resetScoreButton.image.enabled = false;
		resetScoreButton.enabled = false;

		finalScoreText.GetComponent<Text> ().enabled = false;
		highScoreText.GetComponent<Text> ().enabled = false;
	}

	public void Restart()
	{
		Start ();
		GameObject.Find ("FadeWall").GetComponent<FadeWall> ().Restart ();
	}

	public void GoToMenu()
	{
		Application.LoadLevel ("Main_Menu");
	}

	public void WriteEndScore()
	{
		finalScoreText.GetComponent<Text> ().enabled = true;
		StartCoroutine (TypeInScore ());
	}

	IEnumerator TypeInScore()
	{
		// Appends characters to the text field on an increment to simulate a typing effect.
		endReady = false;
		string tmp = "Bombs Sorted: ";
		string tmp2 = "";
		char [] msg = tmp.ToCharArray ();
		int i = 0;
		while (tmp2 != tmp) 
		{
			tmp2 = tmp2 + "" + msg[i];
			i++;
			finalScoreText.text = tmp2;
			yield return new WaitForSeconds(.1f);
		}
		finalScoreText.text = finalScoreText.text + GameObject.Find ("Score").GetComponent<Score> ().GetScore ();
		endReady = true;
		StartCoroutine (TypeInHighScore ());
	}

	IEnumerator TypeInHighScore()
	{
		// Appends characters to the text field on an increment to simulate a typing effect.
		highScoreText.enabled = true;
		endReady = false;
		string tmp = "High Score: ";
		string tmp2 = "";
		char [] msg = tmp.ToCharArray ();
		int i = 0;
		while (tmp2 != tmp) 
		{
			tmp2 = tmp2 + "" + msg[i];
			i++;
			highScoreText.text = tmp2;
			yield return new WaitForSeconds(.1f);
		}
		highScoreText.text = highScoreText.text + GameObject.Find ("Score").GetComponent<Score> ().GetHighScore ();
		endReady = true;
	}

	IEnumerator NewHighScore()
	{
		// Replaces the number in the high score text. To be used when the high score is broken.
		Debug.Log ("NEW HIGH SCORE");
		highScoreText.text = "High Score: ";
		yield return new WaitForSeconds (1f);
		while (endReady) 
		{
			highScoreText.text = "High Score: " + GameObject.Find ("Score").GetComponent<Score> ().GetHighScore ();
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator WaitForFade()
	{
		// Wait until FadeWall has stopped fading.
		// Then enable the start button.
		while (!GameObject.Find("FadeWall").GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame();
		}
		startButton.gameObject.GetComponent<Image> ().enabled = true;
		startButton.enabled = true;
	}

	public void StartGame()
	{
		StartCoroutine (StartSequence ());
	}

	IEnumerator StartSequence()
	{
		// Wait for FadeWall to fade out completely, then start the game.
		startButton.gameObject.GetComponent<Image> ().enabled = false;
		startButton.enabled = false;
		GameObject.Find ("FadeWall").GetComponent<FadeWall> ().CompleteIntroFade ();
		while (!GameObject.Find("FadeWall").GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame();
		}
		GameObject.Find ("GameManager").GetComponent<GameManager> ().StartGame ();
	}

	public void EndGame()
	{
		StartCoroutine (EndSequence ());
	}

	IEnumerator EndSequence()
	{
		// Make FadeWall opaque in preperation for a new game, or a transition to the menu.
		GameObject.Find ("FadeWall").GetComponent<FadeWall> ().FadeOut ();
		while (!GameObject.Find("FadeWall").GetComponent<FadeWall>().ready) 
		{
			yield return new WaitForEndOfFrame();
		}
		Score score = GameObject.Find ("Score").GetComponent<Score>();

		while (!endReady) 
		{
			yield return new WaitForEndOfFrame();
		}

		// We had a new high score, notify the player with a sound an a score update.
		if (score.CheckScore ()) 
		{
			StartCoroutine (NewHighScore ());
			gameObject.GetComponent<AudioSource>().Play();
		}

		while (!endReady) 
		{
			yield return new WaitForEndOfFrame();
		}

		replayButton.image.enabled = true;
		replayButton.enabled = true;

		menuButton.image.enabled = true;
		menuButton.enabled = true;

		resetScoreButton.image.enabled = true;
		resetScoreButton.enabled = true;
	}
}
