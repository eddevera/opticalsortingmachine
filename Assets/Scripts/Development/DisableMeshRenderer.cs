﻿using UnityEngine;
using System.Collections;

public class DisableMeshRenderer : MonoBehaviour 
{
	void Start () 
	{
		gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}
}
