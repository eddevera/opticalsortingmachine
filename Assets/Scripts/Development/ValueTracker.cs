﻿/* For DevUI Group
 * Note: DevUI needs to use Main Camera in the primary UI.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ValueTracker : MonoBehaviour 
{
	public Text bombsDefused;
	public Text scoreBlack;
	public Text scoreRed;
	public Text nextCycleAt;
	public Text scoreTotal;

	public GameObject gameManager;
	public GameObject areaBlack;
	public GameObject areaRed;

	void Update () 
	{
		bombsDefused.text = "BOMBS DEFUSED: " + gameManager.GetComponent<GameManager> ().bombsDefused;
		scoreBlack.text = "SCORE BLACK: " + areaBlack.GetComponent<AreaGeneric> ().count;
		scoreRed.text = "SCORE RED: " + areaRed.GetComponent<AreaGeneric> ().count;
		nextCycleAt.text = "NEXT CYCLE AT: " + gameManager.GetComponent<GameManager> ().rampInterval;
		scoreTotal.text = "SCORE TOTAL: " + GameObject.Find ("Score").GetComponent<Score> ().GetScore ();
	}
}
