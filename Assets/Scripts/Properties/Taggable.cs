﻿/* For determining if a bomb has been placed in a valid bin or back into the arena.
 */

using UnityEngine;
using System.Collections;

public class Taggable : MonoBehaviour 
{
	public string myType;
	public string arenaType;
	public string gameManager;
	public string tagType;

	void Start()
	{
		tagType = arenaType;
	}

	public void Tag(string tag)
	{
		tagType = tag;
	}
	void Initiate(string tag)
	{
		myType = tag;
	}

	public void CheckType()
	{
		if (tagType != myType && tagType != arenaType)
			GameObject.Find (gameManager).SendMessage ("Lose", tagType);
		else if (tagType == arenaType)
			Debug.Log ("DROPPED IN ARENA");
		else 
		{
			Debug.Log ("VALID BIN");
			GameObject.FindObjectsOfType<GameManager>()[0].bombsDefused++; // Tell the Game Manager that a bomb has been defused so we can change the cycle later.
			gameObject.tag = "bomb_disarmed";
			gameObject.GetComponent<Grabbable>().isActive = false;
			gameObject.SendMessage("SetMovementMultiplier",1f);
		}

	}
}
