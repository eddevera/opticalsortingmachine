﻿/* Controls countdowns, critical states and explosions of bombs.
 */

using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour 
{
	public GameObject effectExplosion;
	public Transform spawnPoint;
	public float fuseTime;
	public string gameManager;
	public AudioClip soundBoop;

	bool critical = false;
	static Quaternion dummy = new Quaternion();

	void Start()
	{
		StartCoroutine (Countdown (fuseTime));
	}

	IEnumerator Boop()
	{
		// While the bomb is in a critical state repeatedly play an irritating boop noise.
		// This is timed so you get about two boops before the explosion.
		// By the third boop it's too late.
		while (critical) 
		{
			if(gameObject.tag != "paused" && gameObject.tag != "bomb_disarmed")
			{
				PlaySound (soundBoop);
				yield return new WaitForSeconds (.7f);
			}
			else
				yield return new WaitForSeconds(0);
		}
	}

	IEnumerator CriticalState(int i)
	{
		// Shift the color of the bomb when you have approximately 1.5.
		// If the bomb is disarmed within this time the color freezes and the booping stops.
		StartCoroutine (Boop());
		float duration = fuseTime / 20 * 3;
		while (critical) 
		{
			if(gameObject.tag != "paused")
			{
				Color old = gameObject.GetComponent<SpriteRenderer> ().color;
				Color tmp = Color.Lerp (old, Color.white, Time.deltaTime * duration);
				gameObject.GetComponent<SpriteRenderer>().color = tmp;
				yield return new WaitForSeconds(0);
			}
			else
				yield return new WaitForSeconds(0);
		}
	}

	IEnumerator Countdown(float time)
	{
		// While we're not paused wait for a fraction of fuseTime and check if we've been disarmed.
		// If we're past a certain fraction of the total wait time go critical.
		float interval = time / 20;
		for (int i = 0; i < 20; i++) 
		{
			if(gameObject.tag == "paused")
			{
				i--;
				yield return new WaitForSeconds(0.01f);
			}
			if(i >= 14 && critical == false)
			{
				Debug.Log ("GOING CRITICAL");
				critical = true;
				StartCoroutine(CriticalState(i));
			}
			if(gameObject.tag == "bomb_disarmed")
			{
				critical = false;
				break;
			}
			yield return new WaitForSeconds(interval);
		}
		if (gameObject.tag != "bomb_disarmed") {
			GameObject.Find (gameManager).SendMessage ("Lose", "Arena");
			Explode ();
		} else
			critical = false;
	}

	void PlaySound(AudioClip sound)
	{
		AudioSource tmp = gameObject.GetComponent<AudioSource> ();
		tmp.clip = sound;
		tmp.Play ();
	}

	void Explode()
	{
		// Create an explosion and destroy this object.
		Instantiate (effectExplosion, spawnPoint.position, dummy);
		Destroy (gameObject);
	}
}
