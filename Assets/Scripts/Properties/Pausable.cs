﻿/* For use with the pausing key, not implemented completely.
 * It is functional though.
 */

using UnityEngine;
using System.Collections;

public class Pausable : MonoBehaviour 
{
	string tmp;
	void Pause()
	{
		tmp = gameObject.tag;
		gameObject.tag = "paused";
	}

	void Unpause()
	{
		gameObject.tag = tmp;
	}
}
