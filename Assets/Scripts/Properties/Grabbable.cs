﻿using UnityEngine;
using System.Collections;

public class Grabbable : MonoBehaviour {
	
	Vector3 screenPoint;
	public bool isActive = true;
	
	public KeyCode keyBlack;		// If this key is held the clicked bomb will be sent to the black bin.
	public KeyCode keyRed;			// If this key is held the clicked bomb will be sent to the red bin.

	public KeyCode keyMouseButtonMove;	// If this key is held we will use One-Click Moving without having the setting enabled.

	public bool oneClickMove = false;

	void Awake()
	{
		// Get our preference.
		if (PlayerPrefs.GetInt ("oneClickMove") != 0) 
		{
			oneClickMove = true;
		}
	}

	void Update()
	{
		// Hardcoding stuff for time's sake, ideally this would work with more than two types of areas/bombs.
		// If the key key is down and the bomb isn't grabbed move the bomb to the bin immediately.
		if (!Input.GetKey (keyMouseButtonMove) && !oneClickMove) {
			if (Input.GetKey (keyBlack) && gameObject.tag == "bomb_grabbed") {
				isActive = false;
				gameObject.GetComponent<Taggable> ().tagType = "Black";
				gameObject.transform.position = GameObject.Find ("AutoMove_Black").transform.position;
				gameObject.GetComponent<Taggable> ().CheckType ();
			}
			if (Input.GetKey (keyRed) && gameObject.tag == "bomb_grabbed") {
				isActive = false;
				gameObject.GetComponent<Taggable> ().tagType = "Red";
				gameObject.transform.position = GameObject.Find ("AutoMove_Red").transform.position;
				gameObject.GetComponent<Taggable> ().CheckType ();
			}
		}
	}

	void OnMouseOver()
	{
		// If we're holding the specified button or we have oneClickMove enabled and we're not paused.
		if ((oneClickMove || Input.GetKey(keyMouseButtonMove)) && isActive && gameObject.tag != "paused") 
		{
			if (Input.GetMouseButtonDown (0)) // Left click sends the bomb to the black bin.
			{
				isActive = false;
				gameObject.GetComponent<Taggable> ().tagType = "Black";
				gameObject.transform.position = GameObject.Find ("AutoMove_Black").transform.position;
				gameObject.GetComponent<Taggable> ().CheckType ();
			}
			if (Input.GetMouseButtonDown (1)) // Right click sends the bomb to the red bin.
			{
				isActive = false;
				gameObject.GetComponent<Taggable> ().tagType = "Red";
				gameObject.transform.position = GameObject.Find ("AutoMove_Red").transform.position;
				gameObject.GetComponent<Taggable> ().CheckType ();
			}
		}
	}
	
	void OnMouseDown()
	{
		// If we have an armed bomb and the game isn't paused let the bomb be grabbed.
		if(isActive && gameObject.tag != "paused")
		{
			if(!Input.GetKey (keyMouseButtonMove))
			{
				gameObject.tag = "bomb_grabbed";
				screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			}
		}
	}
	
	void OnMouseUp()
	{
		// Release the held bomb.
		if(isActive && gameObject.tag != "paused")
		{
			if(!Input.GetKey(keyMouseButtonMove))
			{
				gameObject.tag = "bomb_armed";
				gameObject.SendMessage("CheckType");
			}
		}
	}
	
	void OnMouseDrag()
	{
		// Move the held bomb with the cursor.
		if(isActive && gameObject.tag != "paused")
		{
			if(!Input.GetKey (keyMouseButtonMove))
			{
				// Constantly teleport the bomb to the cursor's location.
				Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
				Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
				transform.position = curPosition; 
			}
		}
	}
}
